package com.example.al.canvas;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);

        setContentView(new DrawView(this));
    }

    class DrawView extends View {

        Paint paint;
        Rect rect;
        Path path;
        Bitmap bitmap;

        public DrawView(Context context) {
            super(context);

            paint = new Paint();
            rect = new Rect();
            path = new Path();
            bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        }

        @Override
        protected void onDraw(Canvas canvas) {
//            super.onDraw(canvas);
            canvas.drawARGB(100, 127, 0, 0);

            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(10);
            paint.setSubpixelText(true);
            paint.setAntiAlias(true);
            canvas.drawPoint(100, 100, paint);
            canvas.drawCircle(100, 250, 50, paint);

            paint.setColor(Color.RED);
            paint.setStrokeWidth(5);
            canvas.drawLine(150, 100, 200, 200, paint);
            canvas.drawRect(250, 300, 350, 400, paint);

            rect.set(250, 500, 350, 550);
            paint.setColor(Color.BLUE);
            paint.setStrokeWidth(1);
            paint.setStyle(Paint.Style.STROKE);
            canvas.drawRect(rect, paint);

            paint.setTextSize(40);
            paint.setTextAlign(Paint.Align.LEFT);
            canvas.drawText(String.valueOf(getWidth()) + "x" + String.valueOf(getHeight()), 250, 100, paint);

            paint.setTextSize(20);
            path.addCircle(400, 100, 100, Path.Direction.CW);
            canvas.drawTextOnPath("тест тест тест тест тест тест", path, 0, 0, paint);

            canvas.rotate(-45, 50 + (int)(bitmap.getWidth()/2), 550 + (int)(bitmap.getHeight()/2));
            canvas.drawBitmap(bitmap, 50, 550, paint);
//            canvas.restore();

            float[] lines = {50, 350, 100, 350, 100, 350, 150, 400, 150, 400, 50, 450, 50, 450, 100, 500};
            canvas.drawLines(lines, paint);
        }
    }
}
